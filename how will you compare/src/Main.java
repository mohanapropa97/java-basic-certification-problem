public class Main {

    public static boolean compare(int a, int b)
    {
        return a==b;
    }
    public static boolean compare(String a, String b)
    {
        return a.equals(b);
    }

    public static boolean compare(int[] a, int[] b)
    {
        if(a.length != b.length)
            return  false;

        for(int i=0;i<a.length;i++)
        {
            if(a[i] != b[i])
                return false;
        }
        return true;
    }




    public static void main(String[] args) {
        System.out.println(compare(5,5));
        System.out.println(compare("Mohana","Mohana "));

    }
}